FROM ubuntu:15.10


ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get -y upgrade && \
    apt-get -y install supervisor mysql-server openssh-server software-properties-common vim htop net-tools iputils-ping && \
    apt-add-repository --yes ppa:webupd8team/java && apt-get update && \
    echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections  && \
    echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections  && \
    apt-get -y install curl oracle-java8-installer && \
    apt-get clean

RUN mkdir -p /var/run/sshd
RUN mkdir -p ~/.ssh

RUN mysql_install_db

ADD resources/minecraft-start.sh /opt/minecraft-start.sh
ADD resources/supervisord-services.conf /etc/supervisor/conf.d/supervisord-services.conf


EXPOSE 22 25565


# /data contains static files and database
VOLUME ["/data"]

CMD ["supervisord","-n"]
