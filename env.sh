# local configuration
SSH_KEY=$HOME/.ssh/id_rsa.pub

# docker configuration
IMAGE=minecraft
CONTAINER=minecraft
MINECRAFT_PORT=25565
SSH_PORT=25022
NETWORK=minecraft
