#!/bin/bash

source env.sh

if [ ! -z $(docker ps -qf name=$CONTAINER) ]; then
  echo "container seems running"
  docker ps
elif [ ! -z $(docker ps -aqf name=$CONTAINER) ]; then
  docker network create $NETWORK
  docker start $CONTAINER
else
  docker network create $NETWORK
  docker run -d -p $MINECRAFT_PORT:$MINECRAFT_PORT -p $SSH_PORT:22 --net $NETWORK --name $CONTAINER -v $(pwd)/data:/data $IMAGE
fi

